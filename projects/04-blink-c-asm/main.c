// Blinking LED program in AVR C language using avr-libc and a subroutine in
// assembly.
// Jorge Juan-Chico <jjchico@dte.us.es>.
// Initial date: April, 2021.
//
// Periodically changes bit 5 in PORTB (PB5). It makes the built-in LED to
// blink in the Arduino UNO board that uses an ATmega328p. Can be easily
// modified to us a different output bit.
//
// Uses the delay_ms(), an assembly subroutine, for the waiting periods.

// AVR i/o symbols definitions. Defines port and memory names for easier
// programming. Do not write AVR programs without it.
#include <avr/io.h>

// External functions declarations
int delay_ms(int);

// Main program
int main()
{
    DDRB = (1 << PB5);          // set bit 5 in PORTB as output
                                // (the built-in LED in Arduino UNO is here)
    while (1) {
        PORTB = (1 << PB5);     // turn on the LED
        delay_ms(500);          // stay on for 500ms
        PORTB = (0 << PB5);     // turn off the LED
        delay_ms(500);          // stay off for 500 ms
    }                           // repeat forever
}

// EXERCISE
//
// 1. Compile the code using optimizations (add '-Os' to the compiler's
//    command line) and check that the programs still works correctly.
//
// 2. Change the program so that the LED blinks for 1/100th of a second every
//    5 seconds.
//
// 3. (Advanced) One important advantage of the 'delay_ms' subroutine compared
//    to the '_delay_ms' macro in <util/delay.h> is that the macro only works
//    with constant data (values known at compile time) while the subroutine
//    can be called with a variable. Modify the C program so that the LED
//    blinks at an increasing frequency from 1Hz to 100Hz and starts over
//    again.
//
// 4. (Advanced) Change the delay subroutine so that it takes its argument in
//    seconds. Modify the main program to activate the LED for 1s every 5s.
