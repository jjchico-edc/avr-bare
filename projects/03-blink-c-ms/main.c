// Blinking LED program in AVR C language using avr-libc.
// Jorge Juan-Chico <jjchico@dte.us.es>.
// Initial date: April, 2021.
//
// Periodically changes bit 5 in PORTB (PB5). It makes the built-in LED to
// blink in the Arduino UNO board that uses an ATmega328p. Can be easily
// modified to us a different output bit.
//
// Uses the _delay_ms() function from the avr-libc for the waiting periods.

// AVR i/o symbols definitions. Defines port and memory names for easier
// programming. Do not write AVR programs without it
#include <avr/io.h>

// Define the board's CPU frequency in Hz. Depends on the board and chip
// configuration. 'F_CPU' is the standard macro for thin in avr-libc and is
// used some of its time-related functions. The 'UL' suffix indicates the number
// is an 'unsigned long integer' (32 bits).
#define F_CPU 16000000UL

// Includes avr-libc delay functions. See https://nongnu.org/avr-libc/
#include <util/delay.h>

// Main program
int main()
{
    DDRB = (1 << PB5);          // set bit 5 in PORTB as output
                                // (the built-in LED in Arduino UNO is here)
    while (1) {
        PORTB = (1 << PB5);     // turn on the LED
        _delay_ms(500);         // stay on for 500ms
        PORTB = (0 << PB5);     // turn off the LED
        _delay_ms(500);         // stay off for 500 ms
    }                           // repeat forever
}

// EXERCISE
//
// 1. If the code in this example is compiled without optimization options
//    the compiler will issue a warning message becouse the functions in
//    <util/delay.h> are intended to be used with compiler optimizations
//    enabled. Add option '-Os' to the compiler to optimize for code size
//    and get rid of the warning messages. Debug and test the program in
//    a board.
//
// 2. Change the program so that the LED blinks for 1/10th of a second every
//    3 seconds.
//
// 3. (Advanced) Compile the blink version in blink-c with the optimization
//    option above. Debug and/or try it in a board. Does it work as intended?
//    Why?