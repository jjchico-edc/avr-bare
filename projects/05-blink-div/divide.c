// Blinking LED program in AVR assembly using avr-libc. Waiting function
// Jorge Juan-Chico <jjchico@dte.us.es>.
// Initial date: April, 2021.

//
// divide: divide two integer number.
//   return: division in integer format.
//
// first argument (a) comes in registers r25:r24
// second argument (b) comes in registers r23:r22
//
int divide(int a, int b)
{
    return (int)(a/b);
}